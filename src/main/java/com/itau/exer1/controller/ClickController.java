package com.itau.exer1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.exer1.model.Click;
import com.itau.exer1.repository.ClickRepository;

@Controller
public class ClickController {
	
	@Autowired
	ClickRepository clickRepository;
	
	@RequestMapping(path="/click", method=RequestMethod.POST)
	@ResponseBody
	public Click inserirClick(@RequestBody Click click) {
		return clickRepository.save(click);
	}
	
	@RequestMapping(path="/clicks", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Click> getClicks(){
		return clickRepository.findAll();
	}

}
