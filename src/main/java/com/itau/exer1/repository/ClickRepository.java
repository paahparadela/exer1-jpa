package com.itau.exer1.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.exer1.model.Click;

public interface ClickRepository extends CrudRepository<Click, Integer>{

}
